<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CompetitorProduct;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\ValidatePriceHistoryCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ValidatePriceHistoryCsv;

class ProductController extends Controller
{
    const CSV_FOLDER = '/csv/';
    const CSV_DELIMITER = ';';

    public function getProductByPattern(Request $request): JsonResponse
    {
        $pattern = $request->get('pattern') ?? '';
        $products = Product::query()
            ->findByPattern($pattern)
            ->paginate(Product::PRODUCT_PER_PAGE)
            ->setPath(route('product.list'))
            ->appends('pattern', $pattern);

        return response()->json(compact('products'), 200);
    }

    public function productPriceHistory(ValidatePriceHistoryCollection $request, Product $product): JsonResponse
    {
        return response()->json($product->priceHistory($request->get('startDate'), $request->get('endDate')), 200);
    }

    public function productPriceHistoryCsv(ValidatePriceHistoryCsv $request, Product $product)
    {
        $startDate = $request->get('startDate');
        $startDateObject = new \DateTime($startDate);

        $endDate = $request->get('endDate');
        $endDateObject = new \DateTime($endDate);

        $limit = ($startDateObject->diff($endDateObject))->format('%a');
        $productId = $product->id;

        $competitorIds = $request->get('competitorIds');

        $folderPath = self::getPathToStorage() . self::CSV_FOLDER;
        $csvName = uniqid() . time() . '.csv';
        $csvPath = self::CSV_FOLDER . $csvName;
        $csvFullPath = $folderPath . $csvName;

        if (!File::isDirectory($folderPath)) {
            File::makeDirectory($folderPath, 0777, true, true);
        }

        $headersQuery = DB::query();
        $headersQuery->addSelect(DB::raw("'date'"));
        $headersQuery->addSelect(DB::raw("'product'"));


        $productQuery = DB::table(DB::raw("(SELECT DATE_ADD('{$startDate}', INTERVAL (number - 1) DAY) as date FROM numbers) dates"));
        $productQuery->addSelect(DB::raw('dates.date AS dat'));
        $productQuery->addSelect(DB::raw("IFNULL(pr.price,
                                        IFNULL((SELECT price FROM product_price_history pr2 WHERE DATE(pr2.updated_at) >= date AND pr2.product_id = {$productId} LIMIT 1),
                                        (SELECT IFNULL(sale_price, price) as price FROM products WHERE products.id = {$productId}))
                                   ) as price"));

        $productQuery->leftJoin('product_price_history as pr', function ($join) use ($productId) {
            $join->on(DB::raw('DATE(pr.updated_at)'), '=', 'dates.date')
                ->where('pr.product_id', '=', $productId);
        });

        $product->competitors()->each(function (CompetitorProduct $competitor, $index) use (&$headersQuery, &$productQuery, $competitorIds) {

            if (!in_array($competitor->id, $competitorIds)) {
                return true;
            }

            $headersQuery->addSelect(DB::raw("'{$competitor->title}'"));

            $productQuery->addSelect(DB::raw("IFNULL(cpr_{$index}.price,
                                           IFNULL((SELECT cph_{$index}.price FROM competitor_price_history cph_{$index} WHERE DATE(cph_{$index}.updated_at) >= date AND cph_{$index}.competitor_product_id = {$competitor->id} LIMIT 1),
                                               (SELECT price FROM competitor_products cp_{$index} WHERE  cp_{$index}.id = {$competitor->id}))
                                    ) as price_{$competitor->title}"));

            $productQuery->leftJoin("competitor_price_history as cpr_{$index}", function ($join) use ($competitor, $index) {
                $join->on(DB::raw("DATE(cpr_{$index}.updated_at)"), '=', 'dates.date')
                    ->where("cpr_{$index}.competitor_product_id", '=', "{$competitor->id}");
            });
        });

        $productQuery->limit($limit);
        $dataQuery = $headersQuery->unionAll($productQuery);
        $csvDelimiter = self::CSV_DELIMITER;

        $_query = <<<EOT
            SELECT * INTO OUTFILE '{$csvFullPath}' FIELDS TERMINATED BY '{$csvDelimiter}' ENCLOSED BY '"' ESCAPED BY '"' LINES TERMINATED BY '\r\n' FROM ({$dataQuery->toSql()}) as _t
            EOT;

        DB::select(DB::Raw($_query), $dataQuery->getBindings());
        $csvPath = asset(Storage::url($csvPath));

        try {
            DB::select(DB::Raw($_query), $dataQuery->getBindings());
            return response()->json(compact('csvPath'));
        } catch (QueryException $e) {
            $error = $e->getMessage();
            return response()->json(compact('csvPath', 'error'));
        }
    }


    public static function getPathToStorage()
    {
        return Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
    }
}
