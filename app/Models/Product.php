<?php

namespace App\Models;

use DateTime;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class Product
 * @package App\Models
 * @property integer id
 * @property string code
 * @property string title
 * @property integer price
 * @property Collection prices
 * @property Collection competitors
 * @property string json_prices
 * @property integer sale_price
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon deleted_at
 */
class Product extends Orm
{
    use SoftDeletes;

    const PRODUCT_PER_PAGE = 100;

    protected $appends = [
        'prices'
    ];

    public function scopeFindByPattern($query, $pattern)
    {
        return $query->where('title', 'LIKE', '%' . $pattern . '%');
    }

    public function getPricesAttribute()
    {
        return $this->json_prices ? json_decode($this->json_prices) : new Collection();
    }

    public function competitors()
    {
        return $this->hasMany(CompetitorProduct::class);
    }

    public function priceHistory($startDate = NULL, $endDate = NULL)
    {
        $startDateObject = new DateTime($startDate);

        if (!$startDate) {
            $startDateObject->modify('-30 days');
        }

        $startDate = $startDateObject->format("Y-m-d");

        $endDateObject = new DateTime($endDate);

        if (!$endDate) {
            $endDateObject->modify('+30 days');
        }

        $endDate = $endDateObject->format("Y-m-d");

        $limit = ($startDateObject->diff($endDateObject))->format('%a');
        $productId = $this->id;

        $productPricesQuery = "SELECT JSON_ARRAYAGG(JSON_OBJECT('date', date, 'price', price)) FROM (
                                       SELECT  dates.date AS date, IFNULL(pr.price,
                                                                           IFNULL((SELECT price FROM product_price_history pr2 WHERE DATE(pr2.updated_at) >= date AND pr2.product_id = {$productId} LIMIT 1),
                                                                               (SELECT IFNULL(sale_price, price) as price FROM products WHERE products.id = {$productId}))
                                                                    ) as price
                                            FROM (SELECT DATE_ADD('{$startDate}', INTERVAL (number - 1) DAY) as date FROM numbers) dates
                                       LEFT JOIN product_price_history pr ON DATE(pr.updated_at) = dates.date AND pr.product_id = {$productId} LIMIT {$limit}
                                    ) pr4";

        $competitorsPricesQuery = "SELECT JSON_ARRAYAGG(JSON_OBJECT('date', date, 'price', price)) FROM (
                                       SELECT  dates.date AS date, IFNULL(cph2.price,
                                                                           IFNULL((SELECT cph.price FROM competitor_price_history cph WHERE DATE(cph.updated_at) >= date AND cph.competitor_product_id = competitor_products.id LIMIT 1),
                                                                               (SELECT price FROM competitor_products cp WHERE  cp.id = competitor_products.id))
                                                                    ) as price
                                            FROM (SELECT DATE_ADD('{$startDate}', INTERVAL (number - 1) DAY) as date FROM numbers) dates
                                        LEFT JOIN competitor_price_history cph2 ON DATE(cph2.updated_at) = dates.date AND cph2.competitor_product_id = competitor_products.id LIMIT {$limit}
                                    ) pr4";


        $product = Product::query()
            ->select('*')
            ->addSelect(DB::raw("({$productPricesQuery}) as json_prices"))
            ->where('id', $productId)
            ->first();

        $competitors = CompetitorProduct::query()
            ->select('*')
            ->addSelect(DB::raw('true as showInList'))
            ->addSelect(DB::raw("({$competitorsPricesQuery}) as json_prices"))
            ->where('competitor_products.product_id', $productId)
            ->get();

        return compact('product', 'competitors', 'startDate', 'endDate');
    }
}
