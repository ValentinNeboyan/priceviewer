<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class CompetitorProduct
 * @package App\Models
 * @property integer id
 * @property string code
 * @property string title
 * @property integer price
 * @property Collection prices
 * @property string json_prices
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon deleted_
 */
class CompetitorProduct extends Orm
{
    use SoftDeletes;

    protected $appends = [
        'prices'
    ];

    public function getPricesAttribute()
    {
        return $this->json_prices ? json_decode($this->json_prices) : new Collection();
    }
}
