<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateFillProductsTable extends Migration
{
    const PRODUCT_QUANTITY = 50000;
    const PROCEDURE_NAME = 'fillProductTable';
    const MIN_PRICE = 10;
    const MAX_PRICE = 20000;

    const MIN_SALE_PERCENT = 5;
    const MAX_SALE_PERCENT = 50;

    public function up()
    {
        $counter = 1;
        $chunk = 500;
        $quantity = 500;

        DB::statement("DROP procedure IF EXISTS " . self::PROCEDURE_NAME);
        DB::unprepared(file_get_contents(base_path('database/sql/products.sql')));

        while ($counter < self::PRODUCT_QUANTITY) {

            $incomingParams = $quantity . ','
                . self::MIN_PRICE . ','
                . self::MAX_PRICE . ','
                . self::MIN_SALE_PERCENT . ','
                . self::MAX_SALE_PERCENT . ','
                . $counter;

            DB::statement("CALL " . self::PROCEDURE_NAME . "(" . $incomingParams . ")");
            $counter += $chunk;
            $quantity += $chunk;
        }

        DB::statement("DROP procedure IF EXISTS " . self::PROCEDURE_NAME);
    }

    public function down()
    {
        //
    }
}
