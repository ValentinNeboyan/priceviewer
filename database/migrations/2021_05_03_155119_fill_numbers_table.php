<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FillNumbersTable extends Migration
{
    const PROCEDURE_NAME = 'fillNumbersTable';
    const MAX_NUMBERS = 1000;

    public function up()
    {
        try {
            DB::statement("DROP procedure IF EXISTS " . self::PROCEDURE_NAME);
            DB::unprepared(file_get_contents(base_path('database/sql/numbers.sql')));
            DB::statement("CALL " . self::PROCEDURE_NAME . "(" . self::MAX_NUMBERS . ")");
            DB::statement("DROP procedure IF EXISTS " . self::PROCEDURE_NAME);
        } catch (\Illuminate\Database\QueryException $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
