<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateFillCompetitorProductsTable extends Migration
{
    const PROCEDURE_NAME = 'fillCompetitorProductsTable';
    const PRICE_DIFFERENCE_MAX_PERCENT = 1.3;
    const PRICE_DIFFERENCE_MIN_PERCENT = 0.7;
    const MAX_COMPETITORS = 6;

    public function up()
    {
        DB::statement("DROP procedure IF EXISTS " . self::PROCEDURE_NAME);
        DB::unprepared(file_get_contents(base_path('database/sql/competitor_products.sql')));

        $incomingParams = self::MAX_COMPETITORS . ','
            . self::PRICE_DIFFERENCE_MAX_PERCENT . ','
            . self::PRICE_DIFFERENCE_MIN_PERCENT;

        DB::statement("CALL " . self::PROCEDURE_NAME . "(" . $incomingParams . ")");
        DB::statement("DROP procedure IF EXISTS " . self::PROCEDURE_NAME);
    }

    public function down()
    {
        //
    }
}
