CREATE PROCEDURE simulateProductPriceChanges (IN diffPriceMaxPercent INT , IN diffPriceMinPercent INT,IN maxSalePercent INT , IN minSalePercent INT, IN rowsLimit INT, IN fictionDate DATETIME)
BEGIN
SET  @fictionDate = fictionDate;
UPDATE products p1
INNER JOIN (SELECT id FROM products WHERE DATE(updated_at) < DATE(fictionDate) OR updated_at IS NULL  ORDER BY RAND() LIMIT rowsLimit) p2
ON p1.id = p2.id
SET
    sale_price = (CASE
                    WHEN (FLOOR(RAND() * 10) % 2) AND sale_price > 0 THEN sale_price * ((100 - FLOOR((RAND() * (diffPriceMaxPercent - diffPriceMinPercent + 1)) + diffPriceMinPercent)) / 100)
                    WHEN sale_price = 0 THEN IF(FLOOR(RAND() * 10) % 2 > 0, price * (100 - FLOOR((RAND() * (maxSalePercent - minSalePercent + 1)) + minSalePercent)) / 100, 0)
                    ELSE IF(FLOOR(RAND() * 10) % 2 > 0, sale_price, 0) END),

    price = (CASE WHEN  sale_price = 0
            THEN price * ((100 - FLOOR((RAND() * (diffPriceMaxPercent - diffPriceMinPercent + 1)) + diffPriceMinPercent)) / 100)
            ELSE price END);
END

