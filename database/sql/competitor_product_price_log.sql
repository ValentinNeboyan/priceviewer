CREATE TRIGGER logCompetitorProductPrice AFTER UPDATE ON competitor_products
FOR EACH ROW
BEGIN
    IF NEW.price != OLD.price THEN
        INSERT INTO competitor_price_history (competitor_product_id, price, updated_at) VALUES (old.id, old.price, @fictionDate);
    END IF;
END;
