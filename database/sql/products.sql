CREATE PROCEDURE fillProductTable(IN product_quantity INT, IN minPrice INT, IN maxPrice INT, IN minSalePercent INT, IN maxSalePercent INT, IN counter INT)
BEGIN
DECLARE isSale INT  DEFAULT 0;
DECLARE price INT  DEFAULT 0;
DECLARE salePercent INT  DEFAULT 0;
DECLARE salePrice INT DEFAULT 0;
DECLARE string_values TEXT;
SET @productQuery = "INSERT INTO products (title, price, sale_price) VALUES ";
    WHILE (counter <= product_quantity) DO
        SET isSale = FLOOR(RAND() * 10) % 2;
        SET price = FLOOR(RAND() * (maxPrice - minPrice) + minPrice);
        SET salePercent = FLOOR((RAND() * (maxSalePercent - minSalePercent + 1)) + minSalePercent);
        SET salePrice = IF(isSale > 0, price * (100 - salePercent) / 100, 0) ;
        SET string_values =  CONCAT("('", concat('Product_', counter),"','", price,"','", salePrice,"'),");
        SET @productQuery = CONCAT(@productQuery, string_values);
        SET counter = counter + 1;
    END WHILE;
SET @productQuery = TRIM(TRAILING ',' FROM @productQuery);
PREPARE stmt FROM @productQuery;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET @productQuery = "";
END

