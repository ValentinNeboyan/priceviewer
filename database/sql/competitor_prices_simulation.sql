CREATE PROCEDURE simulateCompetitorPriceChanges (IN diffPriceMaxPercent INT , IN diffPriceMinPercent INT, IN rowsLimit INT, IN fictionDate DATETIME)
BEGIN
SET  @fictionDate = fictionDate;
UPDATE competitor_products cp1
INNER JOIN (SELECT id FROM competitor_products WHERE DATE(updated_at) < DATE(fictionDate) OR updated_at IS NULL  ORDER BY RAND() LIMIT rowsLimit) cp2
ON cp1.id = cp2.id
SET
    price = price * ((100 - FLOOR((RAND() * (diffPriceMaxPercent - diffPriceMinPercent + 1)) + diffPriceMinPercent)) / 100);
END

