@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                    <product-view-component
                        :price-history-route="'{{route('product.api.price-history', $product)}}'"
                        :download-csv-route="'{{route('product.price-history.csv', $product)}}'"
                        :product-object="{{$product->toJson()}}"
                        :competitors-collection="{{$competitors->toJson()}}"
                        :start-date="'{{$startDate}}'"
                        :end-date="'{{$endDate}}'"
                    >
                    </product-view-component>
                </div>
            </div>
        </div>
    </div>
@endsection

