@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="body project_report">
                        <div class="table-responsive">
                            @if($products->count())
                                <product-table-component
                                    :search-route="'{{route('product.search')}}'"
                                    :pattern="'{{$pattern ?? ''}}'"
                                    :product-collection="{{$products->toJson()}}"
                                >
                                </product-table-component>
                            @else
                                <tr>
                                    <td>
                                        <div class="badge badge-info"><h2>No products</h2></div>
                                    </td>
                                </tr>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

