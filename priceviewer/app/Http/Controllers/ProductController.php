<?php

namespace App\Http\Controllers;

use App\Models\Product;


class ProductController extends Controller
{
    public function productList()
    {
        $products = Product::query()->paginate(Product::PRODUCT_PER_PAGE);

        return view('product.list', compact('products'));
    }

    public function productView(Product $product)
    {
        return view('product.view', $product->priceHistory());
    }
}
