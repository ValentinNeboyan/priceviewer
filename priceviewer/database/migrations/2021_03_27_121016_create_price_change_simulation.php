<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePriceChangeSimulation extends Migration
{
    const PROCEDURE_NAME = 'simulateProductPriceChanges';
    const MIN_PERCENT_PRICE_CHANGE = 1;
    const MAX_PERCENT_PRICE_CHANGE = 20;

    const MIN_SALE_PERCENT = 5;
    const MAX_SALE_PERCENT = 50;

    const LIMIT = 1000;

    public function up()
    {
        DB::statement("DROP procedure IF EXISTS " . self::PROCEDURE_NAME);
        DB::unprepared(file_get_contents(base_path('database/sql/product_prices_simulation.sql')));

        $daysForChanges = 60;
        $date = (new DateTime())->modify('-30 days');

        while ($daysForChanges != 0) {

            $incomingParams = self::MAX_PERCENT_PRICE_CHANGE . ','
                . self::MIN_PERCENT_PRICE_CHANGE . ','
                . self::MAX_SALE_PERCENT . ','
                . self::MIN_SALE_PERCENT . ','
                . self::LIMIT . ','
                . '"' . $date->format('Y-m-d H:m:s') . '"';

            DB::statement("CALL " . self::PROCEDURE_NAME . "(" . $incomingParams . ")");

            $date->modify('+1 day');
            $daysForChanges--;
        }

        DB::statement("DROP procedure IF EXISTS " . self::PROCEDURE_NAME);
        DB::statement("SET @fictionDate = NOW()");
    }


    public function down()
    {
        //
    }
}
