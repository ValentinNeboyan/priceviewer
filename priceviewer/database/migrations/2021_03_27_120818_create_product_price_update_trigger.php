<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProductPriceUpdateTrigger extends Migration
{
    const TRIGGER_NAME = 'logProductPrice';

    public function up()
    {
        try {
            DB::statement("DROP TRIGGER IF EXISTS " . self::TRIGGER_NAME);
            DB::unprepared(file_get_contents(base_path('database/sql/product_price_log.sql')));
        } catch (\Illuminate\Database\QueryException $e) {
            dump($e->getMessage());
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_price_update_trigger');
    }
}
