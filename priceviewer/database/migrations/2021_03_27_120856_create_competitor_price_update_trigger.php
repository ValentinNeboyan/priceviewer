<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateCompetitorPriceUpdateTrigger extends Migration
{
    const TRIGGER_NAME = 'logCompetitorProductPrice';

    public function up()
    {
        try {
            DB::statement("DROP TRIGGER IF EXISTS " . self::TRIGGER_NAME);
            DB::unprepared(file_get_contents(base_path('database/sql/competitor_product_price_log.sql')));
        } catch (\Illuminate\Database\QueryException $e) {
            dump($e->getMessage());
        }
    }
    public function down()
    {

    }
}
