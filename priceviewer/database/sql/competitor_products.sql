CREATE PROCEDURE fillCompetitorProductsTable (IN maxCompetitors INT, IN diffPriceMaxPercent DOUBLE , IN diffPriceMinPercent DOUBLE)
BEGIN
DECLARE priceDiff DOUBLE DEFAULT 0;
DECLARE counter INT unsigned DEFAULT 1;
DECLARE subQuery LONGTEXT;
SET @competitorQuery = 'INSERT INTO competitor_products (title, product_id, price) ';
    WHILE (counter <= maxCompetitors) DO
        SET priceDiff = RAND() * (diffPriceMaxPercent - diffPriceMinPercent) + diffPriceMinPercent;
        SET subQuery = CONCAT('(SELECT CONCAT("Cmp_", LOWER(title), "_", ', counter,'), id, price * ', priceDiff , ' FROM products) UNION ');
        SET @competitorQuery = CONCAT(@competitorQuery, subQuery);
        SET counter = counter + 1;
    END WHILE;
SET @competitorQuery = TRIM(TRAILING ' UNION ' FROM @competitorQuery);
PREPARE stmt FROM @competitorQuery;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET @competitorQuery = "";
END
