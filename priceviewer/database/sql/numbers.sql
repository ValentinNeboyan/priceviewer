CREATE PROCEDURE fillNumbersTable (IN maxNumbers INT)
BEGIN
DECLARE numberValue INT DEFAULT 1;
DECLARE subQuery LONGTEXT;
SET @competitorQuery = 'INSERT INTO numbers  VALUES';
    WHILE (numberValue <= maxNumbers) DO
        SET subQuery = CONCAT('(', numberValue, '),');
        SET @competitorQuery = CONCAT(@competitorQuery, subQuery);
        SET numberValue = numberValue + 1;
END WHILE;
SET @competitorQuery = TRIM(TRAILING ',' FROM @competitorQuery);
PREPARE stmt FROM @competitorQuery;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET @competitorQuery = "";
END
