CREATE TRIGGER logProductPrice AFTER UPDATE ON products
FOR EACH ROW
BEGIN
    IF NEW.price != OLD.price AND OLD.sale_price = 0 THEN
        INSERT INTO product_price_history (product_id, price, updated_at) VALUES (old.id, old.price, @fictionDate);
    END IF;
    IF NEW.sale_price != OLD.sale_price THEN
        IF OLD.sale_price > 0 THEN
          INSERT INTO product_price_history (product_id, price, updated_at) VALUES (old.id, old.sale_price, @fictionDate);
        ELSE
          INSERT INTO product_price_history (product_id, price, updated_at) VALUES (old.id, old.price, @fictionDate);
        END IF;
    END IF;
END;
