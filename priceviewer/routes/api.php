<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('product/search', [ProductController::class, 'getProductByPattern'])->name('product.search');
Route::post('product/{product}/price-history', [ProductController::class, 'productPriceHistory'])->name('product.api.price-history');
Route::post('product/{product}/view/price-history-csv', [ProductController::class, 'productPriceHistoryCsv'])->name('product.price-history.csv');
